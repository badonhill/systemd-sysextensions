#! /bin/bash
#
# Copyright (c) 2022 Badon Hill Technologies. All rights reserved
#
# Script to build Open Policy Agent (opa) into a systemd-sysext.
#
# This does not download the opa tarball - pass the tarball's path via --tarball

# There are two usecases
#
# 1.  Called as part of a building an ISO to embed into installer
# 2.  Allow for updating the opa extension on live system
#
# This might explain some of the multiple ways to control script
#
#

# Installer writers
#
# If you want to preprocess this script to embed a policy, then replace these strings (using sed)
#   INSTALLER_POLICY
#

_extension=opa

set -eEu -o pipefail

export SRC=
export SYSEXTENSION=
export SRCVER=

SAFE=$(date +'%Y%m%dT%H%M%S.%N')

# ingore unreachability
# shellcheck disable=SC2317
function clean {
    echo "Cleaning up..."

    rm -rf build-root-"${SAFE}"

}

# ingore unreachability
# shellcheck disable=SC2317
function catch_error {

    local -n _bash_lineno="${1:-BASH_LINENO}"
    local _last_command="${2:-${BASH_COMMAND}}"
    local _code="${3:-0}"

    echo "Caught errno $_code at line #$_bash_lineno : $_last_command"

    exit "$_code"
}

trap 'catch_error "BASH_LINENO" "${BASH_COMMAND}" "${?}"' ERR
trap 'clean' EXIT

################################################################
#
# standard helpers
#

function h1 {

    echo ""
    echo "$(date +'%Y-%m-%dT%H:%M:%S') $*"
    echo "$(date +'%Y-%m-%dT%H:%M:%S') $*" | tr '[:print:]' '='
    echo ""

}

function h2 {

    echo ""
    echo "$*"
    echo "$*" | tr '[:print:]' '-'
    echo ""

}

function log {

    now=$(date +"%Y-%m-%dT%H:%M:%S")
    sed -e "s/^/$now /g"
}

################################################################
#
# application helpers
#

function create_helper {

    buildroot=$1

    cat <<- 'EOF' > "${buildroot}"/usr/local/bin/opa.sh
		#! /bin/bash
		#
		# Copyright (c) 2022 Badon Hill Technologies
		#
		# wrapper around opa client to provide shell expansion of
		# variables for systemd.services

		# This file will be read-only
		if [ -r /usr/share/opa/opa ]
		then
		. /usr/share/opa/opa
		fi

		# Allow overriding of read-only defaults...
		if [ -r /etc/default/opa ]
		then
		. /etc/default/opa
		fi

		if [ "$1" = "install" ]
		then
            touch /etc/docker/opa/config/config.yaml

		    docker plugin install \
                --grant-all-permissions \
                --alias opa-docker-authz \
                openpolicyagent/opa-docker-authz-v2:0.8 \
                opa-args="-policy-file /opa/config/config.yaml"
		    st=$?

            if [ "$st" -eq 0 ]; then
                echo '{ "authorization-plugins": ["opa-docker-authz"] }' | tee /etc/docker/daemon.json
                kill -HUP $(pidof dockerd)
            fi
		    exit $st
		fi

		if [ "$1" = "uninstall" ]
		then
		    docker plugin rm opa-docker-authz
		    st=$?
            if [ "$st" -eq 0 ]; then
                rm -f /etc/docker/daemon.json
                kill -HUP $(pidof dockerd)
            fi
		    exit $st
		fi

		exit 1
EOF

    chmod 755 "${buildroot}"/usr/local/bin/opa.sh

}

function prepare {

    buildroot=$1

    rm -rf "${buildroot}" "${SYSEXTENSION}"

    mkdir -p "${buildroot}"/usr/bin/
    mkdir -p "${buildroot}"/usr/local/bin
    mkdir -p "${buildroot}"/usr/lib/extension-release.d
    mkdir -p "${buildroot}"/usr/lib/systemd/system/multi-user.target.wants/
    mkdir -p "${buildroot}"/usr/share/"${_extension}"/

}

function unpack_src {

    buildroot=$1
    src=$2
    dest=$3

    cp -v "${src}" "${buildroot}"/usr/bin/"${dest}"
}

function create_defaults {

    buildroot=$1

    cat <<EOF > "${buildroot}"/usr/share/opa/authz.rego
    package docker.authz

    allow := true
EOF
}

function create_services {

    buildroot=$1

    cat <<EOF > "${buildroot}"/usr/lib/systemd/system/opa-install-plugin.service
[Unit]
Description=OPA Install Plugin
Documentation=https://www.openpolicyagent.org/
Wants=docker.target systemd-sysext.service
After=systemd-sysext.service docker.service
Requires=systemd-sysext.service

[Service]
Type=oneshot
RemainAfterExit=True
#
# /usr/local/bin/opa.sh handles all configuration
# by reading /usr/share/opa/opa and /etc/default/opa
# and supports conditional expansion
#
ExecStart=/usr/local/bin/opa.sh install
ExecStop=/usr/local/bin/opa.sh uninstall
Restart=never
RuntimeDirectory=opa
RuntimeDirectoryMode=0755
StateDirectory=opa
StateDirectoryMode=0700
CacheDirectory=opa
CacheDirectoryMode=0750

[Install]
WantedBy=multi-user.target

EOF
    ln -s ../opa-install-plugin.service "${buildroot}"/usr/lib/systemd/system/multi-user.target.wants/opa-install-plugin.service

}

function create_metadata {

    buildroot=$1

    cat << EOF > "${buildroot}"/usr/lib/extension-release.d/extension-release."${_extension}"-"${SRCVER}"
ID=flatcar
SYSEXT_LEVEL=1.0
EOF
}

function create_squashfs {

    buildroot=$1
    outpath=$2

    find "${buildroot}" -ls

    mksquashfs "${buildroot}" "${outpath}" -reproducible -all-root -info -no-progress -noappend

}

function build_extension {

    h2 "Building extension for $_extension $SRCVER"

    prepare build-root-"${SAFE}"

    unpack_src build-root-"${SAFE}" "${SRC}" "${_extension}"

    create_defaults build-root-"${SAFE}"

    create_helper build-root-"${SAFE}"

    create_services build-root-"${SAFE}"

    create_metadata build-root-"${SAFE}"

    create_squashfs build-root-"${SAFE}" "${SYSEXTENSION}"

}

################################################################
#
# standard argument parsing - ArgBash
#

die() {
    local _ret="${2:-1}"
    test "${_PRINT_HELP:-no}" = yes && print_help >&2
    echo "$1" >&2
    exit "${_ret}"
}

# shellcheck disable=SC2317
begins_with_short_option() {
    local first_option all_short_options='ph'
    first_option="${1:0:1}"
    test "$all_short_options" = "${all_short_options/$first_option/}" && return 1 || return 0
}

_arg_src=""
_arg_extension=""
_arg_src_ver=""

# shellcheck disable=SC2317
print_help() {
    printf 'Usage: %s [--src <arg>] [--extension <arg>] ... [-h|--help]\n' "$0"
    printf '  %s\n' "--src=FILE              : Path to (local) downloaded file"
    printf '  %s\n' "--extension=RAW         : Path to created extension "
    printf '  %s\n' "--src-version=VER       : Override parsing of verson from src"
    printf '  %s\n' "-h, --help              : Prints help"

    printf '\n'

}

# shellcheck disable=SC2317
parse_commandline() {
    while test $# -gt 0; do
        _key="$1"
        case "$_key" in
            --src)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_src="$2"
                shift
                ;;
            --src=*)
                _arg_src="${_key##--src=}"
                ;;
            --extension)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_extension="$2"
                shift
                ;;
            --extension=*)
                _arg_extension="${_key##--extension=}"
                ;;
            --src-version)
                test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
                _arg_src_ver="$2"
                shift
                ;;
            --src-version=*)
                _arg_src_ver="${_key##--src-version=}"
                ;;
            -h | --help)
                print_help
                exit 0
                ;;
            -h*)
                print_help
                exit 0
                ;;
            *)
                _PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
                ;;
        esac
        shift
    done
}

parse_commandline "$@"

SRC="$_arg_src"
SYSEXTENSION="$_arg_extension"
# shellcheck disable=SC2001
SRCVER="${_arg_src_ver:-$(echo "$SRC" | sed -e 's/.*_\([v0-9.]*\)_.*/\1/')}"

if [ -z "${SRCVER}" ]; then

    echo "Missing/could not parse version from src name"
    exit 1
fi

if [ -z "${SYSEXTENSION}" ]; then

    echo "Missing destination."
    exit 1
fi

h1 "Building ${_extension} Extension"

if [ "$SRC" -nt "$SYSEXTENSION" ] || [ "$0" -nt "$SYSEXTENSION" ]; then

    build_extension | log

else
    printf 'No need to build %s-%s extension. Up to date.' "${_extension}" "${SRCVER}" | log
fi

exit 0
